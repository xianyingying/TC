#coding=utf-8
import smtplib
from email.mime.text import MIMEText
import unittest
import util.HTMLTestReportCN
import time,os

#==================定义发送邮件=====================
def send_mail(file_new):
    #发件邮箱
    mail_from = 'testingwtb@126.com'
    #收件邮箱
    mail_to = 'xiaoming@126.com'
    #邮件正文
    f = open(file_new, 'rb')
    mail_body = f.read()
    f.close()
    #邮件标题
    msg = MIMEText(mail_body, _subtype='html', _charset='utf-8')
    msg['Subject'] = u"自动化测试报告"
    #发送时间
    msg['date'] = time.strftime('%a, %d %b %Y %H:%M:%S %z')
    #连接SMTP服务器
    smtp = smtplib.SMTP()
    smtp.connect('smtp.126.com')
    #邮箱用户名密码
    smtp.login('testingwtb@126.com', '123456')
    smtp.sendmail(mail_from, mail_to, msg.as_string())
    smtp.quit()
    print ("邮件已发送")

# =====查找测试报告目录，找到最新生成的测试报告文件====
def send_report(testreport):
    result_dir = 'D:\TC\H5\report\htmlReport'
    lists = os.listdir(result_dir)
    lists.sort(key=lambda fn: os.path.getmtime(result_dir + "\\" + fn))
    print('最新测试生成的测试报告： ' + lists[-1])
    last_report = os.path.join(result_dir, lists[-1])
    print (last_report)
    send_mail(last_report)

#================将用例添加到测试套件=================
def creatsuite():
    testunit = unittest.TestSuite()
    test_dir = 'D:\TC\H5\test_case\models'
    discover = unittest.defaultTestLoader.discover(test_dir, pattern='test*.py', top_level_dir=None)
    for test_case in discover:
        testunit.addTests(test_case)
    return testunit
if __name__ == '__main__':
    report_time = time.strftime("%Y-%m-%d", time.localtime(time.time()))
    filePath = 'D:/TC/H5/report/htmlReport/' + report_time + '.html'
    fp = open(filePath, 'wb')
    runner = util.HTMLTestReportCN.HTMLTestRunner(stream=fp, title='H5自动化测试报告')
    alltestnames = creatsuite()
    runner.run(alltestnames)

#======================发送报告=======================
fp.close() #关闭生成的报
send_report(filePath) #发送报告
