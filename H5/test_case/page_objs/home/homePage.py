# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page

class HomePage(Page):
    '''首页'''
    message_button = (By.CLASS_NAME, "weui-btn_default")
    activity_button=(By.LINK_TEXT,'/activity')
    order_button=(By.LINK_TEXT,'投注记录')
    notice_button=(By.XPATH,'//ul[@class="quickMenu"]/li[3]')
    service_button=(By.LINK_TEXT,'联系客服')
    more_button = (By.LINK_TEXT, '更多')

    hotlist_1=(By.XPATH,'//ul[@class="hotList"]/li[1]')#热门游戏
    hotlist_1_1=(By.XPATH,'//ul[@class="hotList"]/li[1]/ul[1]/li[1]/a')
    hotlist_1_2=(By.XPATH,'//ul[@class="hotList"]/li[1]/ul[1]/li[2]/a')
    hotlist_1_3=(By.XPATH,'//ul[@class="hotList"]/li[1]/ul[1]/li[3]/a')

    hotlist_2=(By.XPATH,'//ul[@class="hotList"]/li[1]')
    hotlist_2_1 =(By.XPATH,'//ul[@class="hotList"]/li[1]/ul[2]/li[1]/a')
    hotlist_2_2=(By.XPATH,'//ul[@class="hotList"]/li[1]/ul[2] /li[2]/a')

    hotlist_3=(By.XPATH,'//ul[@class="hotList"]/li[2]')
    hotlist_3_1=(By.XPATH,'//ul[@class="hotList"]/li[2]/ul[1]/li[1]/a')
    hotlist_3_2=(By.XPATH,'//ul[@class="hotList"]/li[2]/ul[1]/li[2]/a')

    hotlist_4=(By.XPATH,'//ul[@class="hotList"]/li[2]')
    hotlist_4_1=(By.XPATH,'//ul[@class="hotList"]/li[2]/ul[2]/li[1]/a')
    hotlist_4_2=(By.XPATH,'//ul[@class="hotList"]/li[2]/ul[2]/li[2]/a')

    hotlist_5=(By.XPATH,'//ul[@class="hotList"]/li[3]')

    thirdList_1=(By.XPATH,'//div[@class="thirdList"]/nav/span[1]')#真人视讯
    thirdList_1_input=(By.XPATH,'//div[@class="ipt"]/input')#真人视讯输入框
    thirdList_1_button=(By.XPATH, '//div[@class="thirdList"]/li/div/div[2]/button')#真人视讯立即转入按钮

    thirdList_2=(By.XPATH,'//div[@class="thirdList"]/nav/span[2]')#体育博弈
    thirdList_3=(By.XPATH,'//div[@class="thirdList"]/nav/span[3]')#电子游艺


    def __init__(self, driver, base_url="http://m.tctest3.com"):
        Page.__init__(self, driver, base_url)

    def gotoHomePage(self):
        self.driver.get(self.base_url)

    def click_message_btn(self):
        self.click(self.message_button)

