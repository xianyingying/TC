# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep

class RegisterPage(Page):
    '''注册'''

    def __init__(self, driver, base_url="http://m.tctest3.com"):
        Page.__init__(self, driver, base_url)

    def gotoRegisterPage(self):
        self.driver.get(self.base_url)
        sleep(1)

    username_input_field = (By.XPATH,'/html/body/div[1]/div/div[1]/div[2]/div[2]/div[1]/div/div[1]/div[2]/input')
    password_input_field=(By.XPATH,'/html/body/div[1]/div/div[1]/div[2]/div[2]/div[1]/div/div[2]/div[2]/input')
    def input_name_and_pwd(self,name,password):
        self.input_text(self.username_input_field,name)
        sleep(2)
        self.input_text(self.password_input_field,password)

    register_btn=(By.XPATH,'//*[@id="Register"]/div[1]/div[2]/div[2]/div[2]')
    def click_register_btn(self):
        self.click(self.register_btn)
        sleep(1)







    def click_message_btn(self):
        self.click(self.message_button)

