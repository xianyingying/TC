# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from selenium.webdriver.common.touch_actions import TouchActions

class LoginPage(Page):
    # 封装元素对象
    username_input = (By.XPATH, '//input[@placeholder="请输入用户名/手机号"]')
    password_input = (By.XPATH,'//input[@placeholder="请输入密码"]')
    login_button = (By.CSS_SELECTOR, 'div.login-btn')
    tryplay_button =(By.CLASS_NAME,'shiwan')

    def __init__(self, driver, base_url="http://m.tctest3.com"):
        Page.__init__(self, driver, base_url)


    def gotoLoginPage(self):
        self.driver.get(self.base_url)
        sleep(3)

    def slide_down_from_user_pic(self,down):
        driver=self.driver
        user_pic = driver.find_element_by_class_name('user-pic')
        Action = TouchActions(driver)
        """从用户头像向下滑动down像素"""
        Action.scroll_from_element(user_pic,0,down).perform()
        sleep(3)

    def input_username(self, username):
        self.input_text(self.username_input, username)

    def input_password(self, password):
        self.input_text(self.password_input, password)

    def click_login_btn(self):
        self.click(self.login_button)

    def click_tryplay_btn(self):
         self.click(self.tryplay_button)
         sleep(3)