# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from util.Logs import Logger
from selenium.webdriver.common.touch_actions import TouchActions

mylog = Logger(logger='=recharge').getlog()

class RechargePage(Page):
    """充值"""
    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoRechargePage(self):
        self.driver.get(self.base_url)
        sleep(3)

    def back(self):
        driver=self.driver
        driver.back()
        sleep(3)

    def slide_down_from_user_pic(self,down):
        driver=self.driver
        user_pic = driver.find_element_by_class_name('user-pic')
        Action = TouchActions(driver)
        """从用户头像向下滑动down像素"""
        Action.scroll_from_element(user_pic,0,down).perform()
        sleep(3)

    money_input_box=(By.CLASS_NAME,'weui-input')
    def input_recharge_money(self,money):
        self.input_text(self.money_input_box,money)
        sleep(1)

    online_first=(By.XPATH,'//*[@id="Recharge"]/div[2]/div[2]/div[3]/div[1]/div[2]/p/label/span')
    def click_online_first(self):
        self.click(self.online_first)
        sleep(2)

    online_second=(By.XPATH,'//*[@id="Recharge"]/div[2]/div[2]/div[3]/div[1]/div[2]/p/label/span')
    def click_online_second(self):
        self.click(self.online_second)
        sleep(2)

    first_channel=(By.XPATH,'//ul[@class="list"]/li[1]')
    def click_first_channel(self):
        self.click(self.first_channel)
        sleep(2)

    online_submit_btn=(By.XPATH,'//*[@id="Online"]/section/div/button')
    def click_online_submit_btn(self):
        self.click(self.online_submit_btn)
        sleep(3)

    offline_first=(By.XPATH,'//*[@id="Recharge"]/div[2]/div[3]/div[3]/div[1]/div[2]/p/label/span')
    def click_offline_first(self):
        self.click(self.offline_first)
        sleep(2)

    select_bank_btn=(By.CLASS_NAME,'vux-cell-box')
    sure_btn=(By.CLASS_NAME,'vux-popup-header-right')
    def select_bank(self):
        self.click(self.select_bank_btn)
        sleep(2)
        self.click(self.sure_btn)
        sleep(2)



    name_input_field=(By.XPATH,'/html/body/div[1]/div/section/ul/li[2]/div/div[2]/input')
    def input_name(self,name):
        self.input_text(self.name_input_field,name)



    offline_submit_btn=(By.XPATH,'//section[@class="bankFromList"]/button')
    def click_offline_submit_btn(self):
        self.click(self.offline_submit_btn)
        sleep(2)
























