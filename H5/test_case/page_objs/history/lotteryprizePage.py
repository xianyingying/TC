# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from util.Logs import Logger
from selenium.webdriver.common.touch_actions import TouchActions

mylog = Logger(logger='=lotteryPrizePage').getlog()


class LotteryPrizePage(Page):
    """历史走势"""
    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoLotteryPrizePage(self):
        self.driver.get(self.base_url)
        sleep(3)

    def slide_down_from_ffc(self,down):
        driver=self.driver
        ffc = driver.find_element_by_xpath('//span[text()="分分彩"]')
        Action = TouchActions(driver)
        """从分分彩向下滑动down像素"""
        Action.scroll_from_element(ffc,0,down).perform()
        sleep(3)

    def back(self):
        driver=self.driver
        driver.back()
        sleep(3)


##############################################################################彩种入口
    bjrb_trend_btn = (By.XPATH, "//span[text()='北京幸运28']")
    def click_bjrb_trend_btn(self):
        self.click(self.bjrb_trend_btn)

    xyrb_trend_btn=(By.XPATH,"//span[text()='悉尼幸运28']")
    def click_xyrb_trend_btn(self):
        self.click(self.xyrb_trend_btn)


    jndrb_trend_btn=(By.XPATH,"//span[text()='加拿大幸运28']")
    def click_jndrb_trend_btn(self):
        self.click(self.jndrb_trend_btn)

    twrb_trend_btn=(By.XPATH,"//span[text()='台湾幸运28']")
    def click_twrb_trend_btn(self):
        self.click(self.twrb_trend_btn)

    lhc_trend_btn=(By.XPATH,"//span[text()='香港六合彩']")
    def click_lhc_trend_btn(self):
        self.click(self.lhc_trend_btn)

    jlprb_trend_btn=(By.XPATH,"//span[text()='吉隆坡幸运28']")
    def click_jlprb_trend_btn(self):
        self.click(self.jlprb_trend_btn)

    ffc_trend_btn=(By.XPATH,"//span[text()='分分彩']")
    def  click_ffc_trend_btn(self):
        self.click(self.ffc_trend_btn)

    hlydwf_trend_btn=(By.XPATH,"//span[text()='荷兰1.5分彩']")
    def click_hlydwf_trend_btn(self):
        self.click(self.hlydwf_trend_btn)

    flbsf_trend_btn=(By.XPATH,"//span[text()='菲律宾三分彩']")
    def click_flbsf_trend_btn(self):
        self.click(self.flbsf_trend_btn)

    flbwf_trend_btn=(By.XPATH,"//span[text()='菲律宾五分彩']")
    def click_flbwf_trend_btn(self):
        self.click(self.flbwf_trend_btn)

    wfc_trend_btn=(By.XPATH,"//span[text()='五分彩']")
    def click_wfc_trend_btn(self):
        self.click(self.wfc_trend_btn)

    cqssc_trend_btn=(By.XPATH,"//span[text()='重庆时时彩']")
    def click_cqssc_trend_btn(self):
        self.click(self.cqssc_trend_btn)

    tjssc_trend_btn=(By.XPATH,"//span[text()='天津时时彩']")
    def click_tjssc_trend_btn(self):
        self.click(self.tjssc_trend_btn)

    xjssc_trend_btn=(By.XPATH,"//span[text()='新疆时时彩']")
    def click_xjssc_trend_btn(self):
        self.click(self.xjssc_trend_btn)

    jxsyxw_trend_btn=(By.XPATH,"//span[text()='江西11选5']")
    def click_jxsyxw_trend_btn(self):
        self.click(self.jxsyxw_trend_btn)

    jssyxw_trend_btn=(By.XPATH,"//span[text()='江苏11选5']")
    def click_jssyxw_trend_btn(self):
        self.click(self.jssyxw_trend_btn)

    gdsyxw_trend_btn=(By.XPATH,"//span[text()='广东11选5']")
    def click_gdsyxw_trend_btn(self):
        self.click(self.gdsyxw_trend_btn)

    sdsyxw_trend_btn=(By.XPATH,"//span[text()='山东11选5']")
    def click_sdsyxw_trend_btn(self):
        self.click(self.sdsyxw_trend_btn)

    ahsyxw_trend_btn=(By.XPATH,"//span[text()='安徽11选5']")
    def click_ahsyxw_trend_btn(self):
        self.click(self.ahsyxw_trend_btn)

    shsyxw_trend_btn=(By.XPATH,"//span[text()='上海11选5']")
    def click_shsyxw_trend_btn(self):
        self.click(self.shsyxw_trend_btn)

    xyft_trend_btn=(By.XPATH,"//span[text()='幸运飞艇']")
    def click_xyft_trend_btn(self):
        self.click(self.xyft_trend_btn)

    jssc_trend_btn=(By.XPATH,"//span[text()='极速赛车']")
    def click_jssc_trend_btn(self):
        self.click(self.jssc_trend_btn)

    bjsc_trend_btn=(By.XPATH,"//span[text()='北京赛车']")
    def click_bjsc_trend_btn(self):
        self.click(self.bjsc_trend_btn)


#############################################################################简单历史开奖页面
    qgc_btn=(By.CLASS_NAME,'right-box')
    def click_qgc_btn(self):
        sleep(2)
        self.click(self.qgc_btn)










