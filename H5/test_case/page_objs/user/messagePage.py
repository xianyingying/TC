# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from util.Logs import Logger
from selenium.webdriver.common.touch_actions import TouchActions

mylog = Logger(logger='=message').getlog()

class MessagePage(Page):
    """消息公告"""
    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoMessagePage(self):
        self.driver.get(self.base_url)
        sleep(3)

    def back(self):
        driver=self.driver
        driver.back()
        sleep(3)

    def slide_down_from_user_pic(self,down):
        driver=self.driver
        user_pic = driver.find_element_by_class_name('user-pic')
        Action = TouchActions(driver)
        """从用户头像向下滑动down像素"""
        Action.scroll_from_element(user_pic,0,down).perform()
        sleep(3)


    xiaoxi=(By.XPATH,'//div[@class="vux-tab"]/div[1]')   #消息
    gonggao=(By.XPATH,'//div[@class="vux-tab"]/div[2]')  #公告
    # gengduo=(By.CLASS_NAME,'icon-gengduo')   #更多
    gengduo=(By.XPATH,"/html/body/div[1]/div/div[1]/div[2]/i")
    all_signs_read_btn=(By.XPATH,'//div[@class="batchHandleBox"]/p[1]')  #全部标志为已读
    delete_all_btn=(By.XPATH,'//div[@class="batchHandleBox"]/p[2]')  #全部删除
    cancel_btn=(By.XPATH,'//div[@class="batchHandleBox"]/p[3]')   #取消
    sure_btn=(By.XPATH,'/html/body/div[4]/div/div[2]/div[3]/a[2]')
    def all_massage_are_marked_as_read(self):
        """标志全部消息为已读"""
        self.click(self.gengduo)
        sleep(3)
        self.click(self.all_signs_read_btn)
        sleep(5)

    def delete_all_message(self):
        """删除全部消息"""
        self.click(self.gengduo)
        sleep(3)
        self.click(self.delete_all_btn)
        sleep(2)
        self.click(self.sure_btn)
        sleep(3)
