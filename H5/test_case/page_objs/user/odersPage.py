# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from util.Logs import Logger
from selenium.webdriver.common.touch_actions import TouchActions
from H5.test_case.models.home.test_login import TestLogin


mylog = Logger(logger='=oders').getlog()

class OrdersPage(Page):
    """投注记录"""
    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoOrdersPage(self):
        self.driver.get(self.base_url)
        sleep(3)

    def back(self):
        driver=self.driver
        driver.back()
        sleep(3)

    def slide_down_from_user_pic(self,down):
        driver=self.driver
        user_pic = driver.find_element_by_class_name('user-pic')
        Action = TouchActions(driver)
        """从用户头像向下滑动down像素"""
        Action.scroll_from_element(user_pic,0,down).perform()
        sleep(3)


    type=(By.CLASS_NAME,'text')
    def click_type(self):
        self.click(self.type)

    caipiao=(By.XPATH,'//ul[@class="firstMenu"]/li[1]')
    def click_caipiao(self):
        self.click(self.caipiao)

    shixun=(By.XPATH,'//ul[@class="firstMenu"]/li[2]')
    def click_shixun(self):
        self.click(self.shixun)

    tiyu=(By.XPATH,'//ul[@class="firstMenu"]/li[3]')
    def click_tiyu(self):
        self.click(self.tiyu)

    dianzi=(By.XPATH,'//ul[@class="firstMenu"]/li[4]')
    def click_dianzi(self):
        self.click(self.dianzi)

    tequan=(By.CLASS_NAME,"vux-header-right")
    def click_tequan(self):
        self.click(self.tequan)
        sleep(2)



    start_time=(By.XPATH,'//*[@id="vux-datetime-m0brd"]/div[2]/span')
    end_time=(By.XPATH,'//*[@id="vux-datetime-l8s31"]/div[2]/span')

    quanbu=(By.XPATH,'//div[@class="vux-checker-box"]/div[1]')
    bjrb=(By.XPATH,'//div[@class="vux-checker-box"]/div[2]')
    xnrb=(By.XPATH,'//div[@class="vux-checker-box"]/div[3]')
    jndrb=(By.XPATH,'//div[@class="vux-checker-box"]/div[4]')
    twrb=(By.XPATH,'//div[@class="vux-checker-box"]/div[5]')
    lhc=(By.XPATH,'//div[@class="vux-checker-box"]/div[6]')
    jlprb=(By.XPATH,'//div[@class="vux-checker-box"]/div[7]')
    ffc=(By.XPATH,'//div[@class="vux-checker-box"]/div[8]')
    hlydwf=(By.XPATH,'//div[@class="vux-checker-box"]/div[9]')
    flbsf=(By.XPATH,'//div[@class="vux-checker-box"]/div[10]')
    flbwf=(By.XPATH,'//div[@class="vux-checker-box"]/div[11]')
    wf=(By.XPATH,'//div[@class="vux-checker-box"]/div[12]')
    cqssc=(By.XPATH,'//div[@class="vux-checker-box"]/div[13]')
    tjssc=(By.XPATH,'//div[@class="vux-checker-box"]/div[14]')
    xjssc=(By.XPATH,'//div[@class="vux-checker-box"]/div[15]')
    jxsyxw=(By.XPATH,'//div[@class="vux-checker-box"]/div[16]')
    jssyxw=(By.XPATH,'//div[@class="vux-checker-box"]/div[17]')
    gdsyxw=(By.XPATH,'//div[@class="vux-checker-box"]/div[18]')
    sdsyxw=(By.XPATH,'//div[@class="vux-checker-box"]/div[19]')
    ahsyxw=(By.XPATH,'//div[@class="vux-checker-box"]/div[20]')
    shsyxw=(By.XPATH,'//div[@class="vux-checker-box"]/div[21]')
    xyft=(By.XPATH,'//div[@class="vux-checker-box"]/div[22]')
    jssc=(By.XPATH,'//div[@class="vux-checker-box"]/div[23]')
    bjsc=(By.XPATH,'//div[@class="vux-checker-box"]/div[24]')

    ag=(By.XPATH,'//*[@id="Order"]/ul/li[1]/span')
    lebo=(By.XPATH,'//*[@id="Order"]/ul/li[2]/span')

    sbty=(By.XPATH,'//*[@id="Order"]/ul/li[1]/span')
    sbbench=(By.XPATH,'//*[@id="Order"]/ul/li[2]/span')

    pt=(By.XPATH,'//*[@id="Order"]/ul/li[1]/span')
    fg=(By.XPATH,'//*[@id="Order"]/ul/li[2]/span')

    reset_btn=(By.CLASS_NAME,"btn-reset")
    sure_btn=(By.CLASS_NAME,"btn-sure")


    zigou=(By.XPATH,'//span[text()="自购"]')
    zhuihao=(By.XPATH,'//span[text()="追号"]')

    def click_tz_record(self):
        "点击投注记录"
        driver=TestLogin.driver
        driver.find_elements_by_class_name('l-name')[1].click()

    def click_zh_record(self):
        "点击追号记录"
        driver=TestLogin.driver
        self.click(self.zhuihao)
        sleep(2)
        driver.find_elements_by_class_name('l-name')[1].click()