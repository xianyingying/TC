# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from util.Logs import Logger
from selenium.webdriver.common.touch_actions import TouchActions

mylog = Logger(logger='=help').getlog()

class GameRulePage(Page):
    """游戏规则"""
    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoGameRulePage(self):
        self.driver.get(self.base_url)
        sleep(3)

    def back(self):
        driver=self.driver
        driver.back()
        sleep(3)

    def slide_down_from_user_pic(self,down):
        driver=self.driver
        user_pic = driver.find_element_by_class_name('user-pic')
        Action = TouchActions(driver)
        """从用户头像向下滑动down像素"""
        Action.scroll_from_element(user_pic,0,down).perform()
        sleep(3)


    user_levrl=(By.CLASS_NAME,"icon-dengji")
    def click_user_level(self):
        self.click(self.user_levrl)

    tequan=(By.CLASS_NAME,"vux-header-right")
    def click_tequan(self):
        self.click(self.tequan)
        sleep(2)
