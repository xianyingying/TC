# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from util.Logs import Logger
from selenium.webdriver.common.touch_actions import TouchActions

mylog = Logger(logger='=vipgrade').getlog()

class VipGradePage(Page):
    """用户等级"""
    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoUserPage(self):
        self.driver.get(self.base_url)
        sleep(3)

    def back(self):
        driver=self.driver
        driver.back()
        sleep(3)

    def slide_down_from_user_pic(self,down):
        driver=self.driver
        user_pic = driver.find_element_by_class_name('user-pic')
        Action = TouchActions(driver)
        """从用户头像向下滑动down像素"""
        Action.scroll_from_element(user_pic,0,down).perform()
        sleep(3)


    user_levrl=(By.CLASS_NAME,"icon-dengji")
    def click_user_level(self):
        self.click(self.user_levrl)

    tequan=(By.CLASS_NAME,"vux-header-right")
    def click_tequan(self):
        self.click(self.tequan)
        sleep(2)


    recharge=(By.CLASS_NAME,"icon-wode-chongzhi1")
    def click_recharge(self):
        self.click(self.recharge)

    hall_rule=(By.LINK_TEXT,"查看我当前等级的回水说明")
    def click_hall_rule(self):
        self.click(self.hall_rule)
        sleep(2)

    rule_filter=(By.CLASS_NAME,"icon-shaixuan-tianchong")
    def click_rule_filter(self):
        self.click(self.rule_filter)
        sleep(2)

    caipiao=(By.XPATH,"//span[text()='彩票']")
    def click_caipiao(self):
        self.click(self.caipiao)

    dianzi=(By.XPATH,"//span[text()='电子']")
    def click_dianzi(self):
        self.click(self.dianzi)

    shixun=(By.XPATH,"//span[text()='视讯']")
    def click_shixun(self):
        self.click(self.shixun)


    tiyu=(By.XPATH,"//span[text()='体育']")
    def click_tiyu(self):
        self.click(self.tiyu)

    xyrb=(By.XPATH,"//span[text()='幸运28类']")
    def click_xyrb(self):
        self.click(self.xyrb)

    ssc=(By.XPATH,"//span[text()='时时彩类']")
    def click_ssc(self):
        self.click(self.ssc)
        sleep(2)

    sc=(By.XPATH,"//span[text()='赛车类']")
    def click_sc(self):
        self.click(self.sc)
        sleep(2)

    ks=(By.XPATH,"//span[text()='快3类']")
    def click_ks(self):
        self.click(self.ks)
        sleep(2)

    syxw=(By.XPATH,"//span[text()='11选5类']")
    def click_syxw(self):
        self.click(self.syxw)
        sleep(2)

    lhc=(By.XPATH,"//span[text()='六合彩']")
    def click_lhc(self):
        self.click(self.lhc)
        sleep(2)

    huishui=(By.XPATH,"//span[text()='回水厅']")
    def click_huishui(self):
        self.click(self.huishui)
        sleep(2)

    baoben=(By.XPATH,"//span[text()='保本厅']")
    def click_baoben(self):
        self.click(self.baoben)
        sleep(2)

    gaopeilv=(By.XPATH,"//span[text()='高赔率厅']")
    def click_gaopeilv(self):
        self.click(self.gaopeilv)
        sleep(2)

    chuantong=(By.XPATH,"//span[text()='传统模式']")
    def click_chuantong(self):
        self.click(self.chuantong)
        sleep(2)

    pt=(By.XPATH,"//span[text()='PT']")
    def click_pt(self):
        self.click(self.pt)
        sleep(2)

    fg=(By.XPATH,"//span[text()='FG']")
    def click_fg(self):
        self.click(self.fg)
        sleep(2)


    ag=(By.XPATH,"//span[text()='AG']")
    def click_ag(self):
        self.click(self.ag)
        sleep(2)

    lebo=(By.XPATH,"//span[text()='LEBO']")
    def click_lebo(self):
        self.click(self.lebo)
        sleep(2)

    sbty=(By.XPATH,"//span[text()='沙巴体育']")
    def click_sbty(self):
        self.click(self.sbty)
        sleep(10)

    sbbench=(By.XPATH,"//span[text()='SBBENCH']")
    def click_sbbench(self):
        self.click(self.sbbench)
        sleep(2)


    chongzhi=(By.XPATH,"//*[@id='HallRule']/div[2]/div/footer/button[1]")
    def click_chongzhi(self):
        self.click(self.chongzhi)

    queren=(By.XPATH,"//*[@id='HallRule']/div[2]/div/footer/button[2]")
    def click_queren(self):
        self.click(self.queren)
        sleep(2)