# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from util.Logs import Logger
from selenium.webdriver.common.touch_actions import TouchActions

mylog = Logger(logger='=user').getlog()

class UserPage(Page):
    """我的页面"""
    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoUserPage(self):
        self.driver.get(self.base_url)
        sleep(3)

    def back(self):
        driver=self.driver
        driver.back()
        sleep(3)

    def slide_down_from_user_pic(self,down):
        driver=self.driver
        user_pic = driver.find_element_by_class_name('user-pic')
        Action = TouchActions(driver)
        """从用户头像向下滑动down像素"""
        Action.scroll_from_element(user_pic,0,down).perform()
        sleep(3)

    lucky_draw=(By.CLASS_NAME,"luckDrawBtn")
    def click_lucky_draw(self):
        self.click(self.lucky_draw)


    recharge=(By.CLASS_NAME,"icon-wode-chongzhi1")
    def click_recharge(self):
        self.click(self.recharge)

    user_level=(By.CLASS_NAME,"icon-dengji")
    def click_user_level(self):
        self.click(self.user_level)

    transfer=(By.CLASS_NAME,"icon-i-money")
    def click_transfer(self):
        self.click(self.transfer)

    funds_details=(By.CLASS_NAME,"icon-wode-jifen")
    def click_funds_details(self):
        self.click(self.funds_details)

    betting_record=(By.CLASS_NAME,"icon-touzhujilu")
    def click_betting_record(self):
        self.click(self.betting_record)

    my_team=(By.CLASS_NAME,"icon-wodetuandui")
    def click_my_team(self):
        self.click(self.my_team)

    message_notification=(By.CLASS_NAME,"icon-xiaoxigonggao")
    def click_message_notification(self):
        self.click(self.message_notification)

    safety_center=(By.CLASS_NAME,"icon-anquan")
    def click_safety_center(self):
        self.click(self.safety_center)

    customer_service=(By.CLASS_NAME,"icon-kefu")
    def click_customer_service(self):
        self.click(self.customer_service)

    rebate=(By.CLASS_NAME,"icon-huishui1")
    def click_rebate(self):
        self.click(self.rebate)

    rules_of_the_game=(By.CLASS_NAME,"icon-guize1")
    def click_rules_of_the_game(self):
        self.click(self.rules_of_the_game)

    setting=(By.CLASS_NAME,"icon-shezhi")
    def click_setting(self):
        self.click(self.setting)
        sleep(2)


    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoUserPage(self):
        self.driver.get(self.base_url)
        sleep(3)





