# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from util.Logs import Logger
from selenium.webdriver.common.touch_actions import TouchActions

mylog = Logger(logger='=team').getlog()

class TeamPage(Page):
    """我的团队"""
    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoTeamPage(self):
        self.driver.get(self.base_url)
        sleep(3)

    def back(self):
        driver=self.driver
        driver.back()
        sleep(3)

    def slide_down_from_user_pic(self,down):
        driver=self.driver
        user_pic = driver.find_element_by_class_name('user-pic')
        Action = TouchActions(driver)
        """从用户头像向下滑动down像素"""
        Action.scroll_from_element(user_pic,0,down).perform()
        sleep(3)

    tjxj_btn=(By.CLASS_NAME,'team-footer')
    def click_tjxj_btn(self):
        """点击添加下级"""
        self.click(self.tjxj_btn)
        sleep(6)

    account=(By.XPATH,'/html/body/div[1]/div/section/ul/li[1]/div/div/div[2]/input')
    def input_account(self):
        """输入账号"""
        self.input_text(self.account, "member1")

    password=(By.XPATH,'/html/body/div[1]/div/section/ul/li[2]/div/div/div[2]/input')
    def input_password(self):
        """输入密码"""
        self.input_text(self.password,"123456")

    repeat_password=(By.XPATH,'/html/body/div[1]/div/section/ul/li[3]/div/div/div[2]/input')
    def input_repeat_password(self):
        """重复密码"""
        self.input_text(self.repeat_password, "123456")

    caipiao_fanyong=(By.XPATH,'/html/body/div[1]/div/section/ul/li[4]/div/div/div[2]/input')
    def input_caipiao_fanyong(self, caipiao_fanyong):
        """输入彩票返佣"""
        self.input_text(self.caipiao_fanyong, caipiao_fanyong)

    shixun_fanyong=(By.XPATH,'/html/body/div[1]/div/section/ul/li[5]/div/div/div[2]/input')
    def input_shixun_fanyong(self, shixun_fanyong):
        """输入视频返佣"""
        self.input_text(self.shixun_fanyong, shixun_fanyong)

    dianzi_fanyong=(By.XPATH,'/html/body/div[1]/div/section/ul/li[6]/div/div/div[2]/input')
    def input_dianzi_fanyong(self, dianzi_fanyong):
        """输入电子返佣"""
        self.input_text(self.dianzi_fanyong,dianzi_fanyong)

    tiyu_fanyong=(By.XPATH,'/html/body/div[1]/div/section/ul/li[7]/div/div/div[2]/input')
    def input_tiyu_fanyong(self, tiyu_fanyong):
        """输入体育返佣"""
        self.input_text(self.tiyu_fanyong, tiyu_fanyong)

    sure_btn=(By.XPATH,'//*[@id="app"]/div/footer/button')
    def click_sure_btn(self):
        """点击确认按钮"""
        self.click(self.sure_btn)
        sleep(3)
