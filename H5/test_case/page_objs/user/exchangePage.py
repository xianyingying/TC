# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from util.Logs import Logger
from selenium.webdriver.common.touch_actions import TouchActions

mylog = Logger(logger='=exchange').getlog()

class ExchangePage(Page):
    """额度转换"""
    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoExchangePage(self):
        self.driver.get(self.base_url)
        sleep(3)

    def back(self):
        driver=self.driver
        driver.back()
        sleep(3)

    transfer_btn=(By.CLASS_NAME,'icon-i-money')
    def click_transfer_btn(self):
        """点击额度转换菜单"""
        self.click(self.transfer_btn)
        sleep(2)


    def slide_down_from_zhuqianbao(self,n):
        driver=self.driver
        zhuqianbao = (By.XPATH, '//*[@id="vux-picker-jdtat-0"]/div/div[3]/div[1]')
        Action = TouchActions(driver)
        """从主钱包向下滑动n*34像素"""
        Action.scroll_from_element(zhuqianbao,0,n*34).perform()
        sleep(3)

    out_wallet=(By.XPATH,"//*[@id='exchange-box']/div[3]/div/div[1]/div[1]")
    def click_out_wallet(self):
        self.click(self.out_wallet)

    in_wallet=(By.XPATH,"//*[@id='exchange-box']/div[3]/div/div[1]/div[2]")
    def click_in_wallet(self):
        self.click(self.in_wallet)
        sleep(5)


    transfer_money=(By.XPATH,'//input[@placeholder="请填写要转换的金额"]')
    def input_transfer_money(self, money):
        self.input_text(self.transfer_money, money)

    submit_btn=(By.CLASS_NAME,"submit")
    def click_submit_btn(self):
        self.click(self.submit_btn)

    qbsh_btn=(By.CLASS_NAME,"weui-btn_mini")
    def click_qbsh_btn(self):
        self.click(self.qbsh_btn)

    # zhuqianbao=(By.XPATH,'//*[@id="vux-picker-jdtat-0"]/div/div[3]/div[1]')
    in_zhuqianbao=(By.XPATH,'//*[@id="vux-picker-zaxjb-0"]/div/div[2]')
    out_zhuqianbao=(By.XPATH,'/html/body/div[6]/div/div/div[2]/div/div/div/div/div[2]')
    def click_out_zhuqianbao(self):
        #点击主钱包
        self.click(self.out_zhuqianbao)
        sleep(10)

    in_fgqianbao=(By.XPATH,'//*[@id="vux-picker-jdtat-0"]/div/div[3]/div[2]')
    def click_fgqianbao(self):
        #点击FG钱包
        self.click(self.fgqianbao)

    in_leboqianbao=(By.XPATH,'//*[@id="vux-picker-jdtat-0"]/div/div[3]/div[3]')
    def click_leboqianbao(self):
        #点击LEBO钱包
        self.click(self.leboqianbao)

    ptqianbao=(By.XPATH,'//*[@id="vux-picker-jdtat-0"]/div/div[3]/div[4]')
    def click_ptqianbao(self):
        #点击PT钱包
        self.click(self.ptqianbao)

    sbbqianbao=(By.XPATH,'//*[@id="vux-picker-jdtat-0"]/div/div[3]/div[5]')
    def click_sbbqianbao(self):
        #点击SBBENCH钱包
        self.click(self.sbbqianbao)

    sbqianbao=(By.XPATH,'//*[@id="vux-picker-jdtat-0"]/div/div[3]/div[6]')
    def click_sbqianbao(self):
        #点击沙巴体育钱包
        self.click(self.sbqianbao)

    quxiao=(By.CLASS_NAME,"vux-popup-header-left")
    def click_quxiao(self):
        #点击取消按钮
        self.click(self.quxiao)

    # queding=(By.CLASS_NAME,"vux-popup-header-right")
    # queding=(By.CSS_SELECTOR,'.vux-popup-header-right')
    queding=(By.XPATH,'/html/body/div[6]/div/div/div[1]/div[3]')
    def click_queding(self):
        #点击确定按钮
        self.click(self.queding)


