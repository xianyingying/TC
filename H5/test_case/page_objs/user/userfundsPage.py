# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from util.Logs import Logger
from selenium.webdriver.common.touch_actions import TouchActions

mylog = Logger(logger='=userfunds').getlog()

class UserFundsPage(Page):
    """资金明细"""
    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoUserFundsPage(self):
        self.driver.get(self.base_url)
        sleep(3)

    def back(self):
        driver=self.driver
        driver.back()
        sleep(3)

    def slide_down_from_user_pic(self,down):
        driver=self.driver
        user_pic = driver.find_element_by_class_name('user-pic')
        Action = TouchActions(driver)
        """从用户头像向下滑动down像素"""
        Action.scroll_from_element(user_pic,0,down).perform()
        sleep(3)


    filter=(By.CLASS_NAME,"icon-shaixuan-tianchong")
    def click_filter(self):
        #点击筛选按钮
        self.click(self.filter)
        sleep(2)

    start_time=(By.XPATH,'//*[@id="vux-datetime-chzmv"]/div[2]/span')
    def click_start_time(self):
        #点击开始时间
        self.click(self.start_time)

    end_time=(By.XPATH,'//*[@id="vux-datetime-49xv5"]/div[2]/span')
    def click_end_time(self):
        #点击结束时间
        self.click(self.end_time)

    all=(By.XPATH,'//div[@class="vux-checker-box"]/div[1]')
    def click_all(self):
        #点击全部
        self.click(self.all)

    income=(By.XPATH,'//div[@class="vux-checker-box"]/div[2]')
    def click_income(self):
        #点击收入
        self.click(self.income)
        sleep(1)

    expend=(By.XPATH,'//div[@class="vux-checker-box"]/div[3]')
    def click_expend(self):
        #点击支出
        self.click(self.expend)

    transfer=(By.XPATH,'//div[@class="vux-checker-box"]/div[4]')
    def click_transfer(self):
        #点击额度转换
        self.click(self.transfer)

    reset_btn=(By.CLASS_NAME,"btn-reset")
    def click_reset_btn(self):
        #点击重置按钮
        self.click(self.reset_btn)

    sure_btn=(By.CLASS_NAME,"btn-sure")
    def click_sure_btn(self):
        #点击确认按钮
        self.click(self.sure_btn)



