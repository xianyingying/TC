# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from util.Logs import Logger
from selenium.webdriver.common.touch_actions import TouchActions

mylog = Logger(logger='=safety').getlog()

class SafetyPage(Page):
    """安全中心"""
    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoTeamPage(self):
        self.driver.get(self.base_url)
        sleep(3)

    submit_btn=(By.CLASS_NAME,'submit-btn')  #提交按钮

    smrz=(By.LINK_TEXT,'未认证')  #实名认证
    name=(By.XPATH,'//input[@placeholder="请输入真实姓名"]')
    id_number=(By.XPATH,'//input[@placeholder="请输入证件号码"]')
    def id_auth(self,name,id_number):
        self.click(self.smrz)
        sleep(3)
        self.input_text(self.name,name)
        self.input_text(self.id_number, id_number)
        sleep(2)
        self.click(self.submit_btn)
        sleep(2)

    dlmm=(By.LINK_TEXT,'修改')  #登录密码修改
    old_pwd=(By.XPATH,'//input[@placeholder="请输入旧密码"]')
    new_pwd=(By.XPATH,'/html/body/div[1]/div/div[2]/div/div[2]/div/div[2]/div[2]/input')
    confirm_pwd=('/html/body/div[1]/div/div[2]/div/div[2]/div/div[3]/div[2]/input')

    def reset_pwd(self,old_pwd,new_pwd):
        """重置密码"""
        self.click(self.dlmm)
        sleep(3)
        self.input_text(self.old_pwd,old_pwd)
        self.input_text(self.new_pwd, new_pwd)
        self.input_text(self.confirm_pwd, new_pwd)
        self.click(self.submit_btn)
        sleep(2)



    txmm=(By.LINK_TEXT,'未设置')  #提现密码
    tx_old_pwd=(By.XPATH,'//input[@placeholder="请输入4位数字"]')
    tx_new_pwd=(By.XPATH,'//input[@placeholder="请输入4位数字"]')
    def set_tixian_pwd(self,tx_old_pwd,tx_new_pwd):
        """设置提现密码"""
        self.click(self.txmm)
        sleep(3)
        self.input_text(self.tx_old_pwd,tx_old_pwd)
        self.input_text(self.tx_new_pwd, tx_new_pwd)
        self.click(self.submit_btn)
        sleep(2)


    sjbd=(By.LINK_TEXT,'手机绑定')  #手机绑定
    phone_num=()
    phone_vrf_code=()
    def binding_phone_number(self,phone_num,phone_vrf_code):
        """绑定手机号"""
        self.click(self.sjbd)
        sleep(3)
        self.input_text(self.phone_num,phone_num)
        self.input_text(self.phone_vrf_code, phone_vrf_code)
        self.click(self.submit_btn)
        sleep(2)

    cyyx=(By.LINK_TEXT,'常用邮箱')  #常用邮箱
    email_add=()
    email_vrf_code = ()
    def test_binding_email_add(self,email_add,email_vrf_code):
        """绑定常用邮箱"""
        self.click(self.cyyx)
        sleep(3)
        self.input_text(self.email_add,email_add)
        self.input_text(self.email_vrf_code, email_vrf_code)
        self.click(self.submit_btn)
        sleep(2)
