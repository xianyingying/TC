# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from util.Logs import Logger
from selenium.webdriver.common.touch_actions import TouchActions

mylog = Logger(logger='=service').getlog()

class ServicePage(Page):
    """联系客服"""
    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoServicePage(self):
        self.driver.get(self.base_url)
        sleep(3)

    def back(self):
        driver=self.driver
        driver.back()
        sleep(3)

    def slide_down_from_user_pic(self,down):
        driver=self.driver
        user_pic = driver.find_element_by_class_name('user-pic')
        Action = TouchActions(driver)
        """从用户头像向下滑动down像素"""
        Action.scroll_from_element(user_pic,0,down).perform()
        sleep(3)

    #问题类型
    wtlx_1=(By.XPATH,'//*[@id="msgList"]/li[3]/div/ul/li[1]')
    wtlx_2=(By.XPATH,'//*[@id="msgList"]/li[3]/div/ul/li[2]')
    wtlx_3=(By.XPATH,'//*[@id="msgList"]/li[3]/div/ul/li[3]')
    wtlx_4=(By.XPATH,'//*[@id="msgList"]/li[3]/div/ul/li[4]')

    text_field=(By.XPATH,'//input[@placeholder="请输入您要咨询的问题"]')  #输入框

    sub_btn=(By.CLASS_NAME,'subBtn')  #发送按钮

    def contact_service(self,text):
        self.click(self.wtlx_1)
        sleep(2)
        self.input_text(self.text_field,text)
        self.click(self.sub_btn)

