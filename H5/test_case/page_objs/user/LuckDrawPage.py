# _*_ coding:utf-8 _*_
from selenium.webdriver.common.by import By
from H5.test_case.page_objs.basePage import Page
from time import sleep
from util.Logs import Logger
from selenium.webdriver.common.touch_actions import TouchActions

mylog = Logger(logger='=luckDraw').getlog()


class LuckyDrawPage(Page):
    """幸运轮盘"""
    def __init__(self, driver, base_url):
        Page.__init__(self, driver, base_url)

    def gotoLuckyDrawPage(self):
        self.driver.get(self.base_url)
        sleep(3)

    def slide_down_from_strat_btn(self,down):
        driver=self.driver
        start_btn = driver.find_element_by_class_name('startBtn')
        Action = TouchActions(driver)
        """从Go向下滑动down像素"""
        Action.scroll_from_element(start_btn,0,down).perform()
        sleep(3)

    def back(self):
        driver=self.driver
        driver.back()
        sleep(3)

    start_btn=(By.CLASS_NAME,"stareBtn")
    def click_start_btn(self):
        self.click(self.start_btn)
        sleep(10)

    winning_list=(By.XPATH,"//div[@class='winningList']/h2")
    def click_winning_list(self):
        self.click(self.winning_list)
        sleep(3)

    close_btn=(By.CLASS_NAME,"closeButton")
    def click_close_btn(self):
        self.click(self.close_btn)

    tip_close_btn=(By.CLASS_NAME,"close")
    def click_tip_close_btn(self):
        self.click(self.tip_close_btn)
        sleep(1)

    tip_yes_btn=(By.CLASS_NAME,"yes")
    def click_tip_yes_btn(self):
        self.click(self.tip_yes_btn)

