import unittest
from time import sleep

from selenium import webdriver
from H5.test_case.page_objs.home.homePage import HomePage
from H5.test_case.page_objs.home.loginPage import LoginPage
from H5.test_case.page_objs.user.userPage import UserPage
from util.Logs import Logger
from util.readJson import readJson

mylog = Logger(logger='=test_login').getlog()

class TestLogin(unittest.TestCase):
    '''登录模块'''
    mobileEmulation = {'deviceName': 'iPhone X'}
    option = webdriver.ChromeOptions()
    option.add_experimental_option('mobileEmulation', mobileEmulation)
    driver = webdriver.Chrome(options=option)
    base_url = "http://m.tctest3.com"
    verificationErrors = []
    accept_next_alert = True

    rj=readJson()
    data = rj.load_data("login.json")
    LP_url = data["URL"]
    LP = LoginPage(driver, LP_url)  # 声明LoginPage对象
    UP_url = "http://m.tcuat0.com/user"
    UP = UserPage(driver, UP_url)  # 声明UP对象
    username = data["userName"]
    password = data["userPwd"]


    def login(self):
        '''登录'''
        driver = self.driver
        rj = readJson()
        data = rj.load_data("login.json")
        url = data["URL"]
        username = data["userName"]
        password = data["userPwd"]
        LP = LoginPage(driver, url)  # 声明LP对象
        LP.gotoLoginPage()  # 调用打开页面组件
        LP.input_username("oookkk")
        LP.input_password("123456")
        LP.click_login_btn()
        sleep(2)
        driver.find_element_by_class_name("weui-btn_default").click()
        sleep(3)

    def test_login(self):
         '''测试登录'''
         self.LP.gotoLoginPage()  #调用打开页面组件
         self.LP.input_username(self.username)
         self.LP.input_password(self.password)
         self.LP.click_login_btn()
         sleep(2)
         self.LP.getImage
         sleep(3)

    def test_tryplay_login(self):
         '''测试试玩登录'''
         self.LP.gotoLoginPage()  #调用打开页面组件
         self.LP.click_tryplay_btn()
         self.LP.getImage
         sleep(3)


    def test_logout(self):
        driver=self.driver
        driver.get("http://m.tcuat0.com")
        driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.LP.slide_down_from_user_pic(900)
        self.UP.click_setting()
        driver.find_element_by_class_name("submit-btn").click()
        sleep(2)
        self.UP.getImage
        sleep(3)



if __name__ == "__main__":
      unittest.main()


