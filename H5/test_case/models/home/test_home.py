import unittest
from time import sleep

from selenium import webdriver

from H5.test_case.page_objs.home.homePage import HomePage
from H5.test_case.page_objs.home.loginPage import LoginPage
from H5.test_case.page_objs.user.LuckDrawPage import LuckyDrawPage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_home').getlog()

class TestHome(unittest.TestCase):
    '''首页'''
    @classmethod
    def setUp(self):
        mobileEmulation = {'deviceName': 'iPhone X'}
        mylog.info("设置手机型号为iPhone X")
        option = webdriver.ChromeOptions()
        option.add_experimental_option('mobileEmulation', mobileEmulation)
        self.driver = webdriver.Chrome( options = option)
        self.base_url="http://m.tcuat0.com"
        mylog.info("打开浏览器")
        self.verificationErrors = []
        self.accept_next_alert = True



    def test_lucky_draw(self):
        '''测试幸运轮盘'''
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/LuckDraw"
        LDP = LuckyDrawPage(driver,url)#声明UP对象
        driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        driver.find_element_by_class_name("luckDrawBtn").click()
        sleep(2)
        LDP.click_start_btn()
        mylog.info("点击幸运轮盘Go按钮，检查轮盘抽奖")
        LDP.click_tip_close_btn()
        LDP.click_winning_list()
        LDP.click_close_btn()
        LDP.back()



    @classmethod
    def tearDown(self):
         self.driver.quit()

if __name__ == "__main__":
      unittest.main()