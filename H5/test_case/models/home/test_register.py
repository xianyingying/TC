import unittest
from H5.test_case.models.home.test_login import TestLogin
from H5.test_case.page_objs.home.registerPage import RegisterPage
from util.Logs import Logger

mylog = Logger(logger='=test_register').getlog()

class TestRegister(unittest.TestCase):
    '''注册'''
    driver = TestLogin.driver
    url = "http://m.tcuat0.com/register"
    RGP = RegisterPage(driver, url)

    def test_register(self):
        '''测试注册'''
        self.RGP.gotoRegisterPage()
        self.RGP.input_name_and_pwd("iuiuiu","123456")
        self.RGP.click_register_btn()
        self.RGP.getImage()


if __name__ == "__main__":
      unittest.main()