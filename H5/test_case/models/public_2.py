import os
import sys
import time
import unittest

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select


#the file is base_page.py
#author=abao
# 定位元素方法
def find_element(self,type,position):
  if type == 'xpath':
    element=self.driver.find_element_by_xpath(position)
    #element_exist(element)
    return element
  elif type == 'id':
    element = self.driver.find_element_by_id(position)
    #element_exist( element )
    return element
  elif type =='name':
    element = self.driver.find_element_by_name(position)
    #element_exist( element )
    return element
  elif type == 'link_text':
    element = self.driver.find_element_by_link_text(position)
    #element_exist( element )
    return element
  else:
    print("不支持的类型")
  '''
  try:
    # 确保元素是可见的。
    # 注意：以下入参为元组的元素，需要加*。Python存在这种特性，就是将入参放在元组里。
    WebDriverWait(self.driver,10).until(lambda driver: element.is_displayed())
    # 注意：以下入参本身是元组，不需要加*
    #WebDriverWait( self.driver, 10 ).until( EC.visibility_of_element_located( loc ) )
    return element
  except:
    print("元素没有出现，等待超时")
            '''
# 定位元素方法
def find_elements(self,type,position):
  if type == 'xpath':
    element=self.driver.find_elements_by_xpath(position)
    #element_exist(element)
    return element
  elif type == 'id':
    element = self.driver.find_elements_by_id(position)
    #element_exist( element )
    return element
  elif type =='name':
    element = self.driver.find_elements_by_name(position)
    #element_exist( element )
    return element
  elif type == 'link_text':
    element = self.driver.find_elements_by_link_text(position)
    #element_exist( element )
    return element
  else:
    print("不支持的类型")


# the file is base_page.py
# author=abao
def element_exist(self, element):
    try:

        element
    except NoSuchElementException as msg:
        print("you find element is not exist%s" % msg)
    if element.is_displayed():
        print("元素可见")
    else:
        print("元素不可见")


#the file is base_page.py
#author=abao
def elements_exist(self, type,position):
  for i in range(5):
    try:
      elements=self.find_elements(type,position)
      num=_len_(elements)
    except:
      time.sleep(3)
    if num >=1:
      break
  display_element=[]
  for j in range(num):
     element=elements._getitem_(j)
     if element.is_displayed():
       display_element.append(element)
  return display_element