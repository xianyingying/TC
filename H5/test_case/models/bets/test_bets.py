import unittest
from time import sleep
from H5.test_case.page_objs.bets.betsPage import BetsPage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_bets').getlog()

class TestBets(unittest.TestCase):
    '''投注追号'''

    driver = TestLogin.driver
    url = "http://m.tcuat0.com/category"
    BTP = BetsPage(driver, url)  # 声明self.BTP对象



###########################################################################################测试北京幸运28投注追号

    def test_bjrb_tz(self):
        """测试传统模式 北京幸运28投注"""
        self.driver.get("http://m.tcuat0.com")
        self.driver.find_element_by_class_name("icon-gc").click()
        sleep(2)
        self.BTP.click_ct_bjrb()
        self.BTP.click_numbers(0)
        sleep(2)
        self.BTP.click_touzhu()
        self.BTP.click_order_record()
        self.BTP.getImage


    def test_bjrb_zh(self):
        """测试传统模式 北京幸运28追号"""
        self.driver.get("http://m.tcuat0.com")
        self.driver.find_element_by_class_name("icon-gc").click()
        sleep(2)
        self.BTP.click_ct_bjrb()
        self.BTP.click_numbers(0)
        sleep(2)
        self.BTP.click_zhuihao_btn()
        self.BTP.click_zhuihao_queren()
        self.BTP.click_order_record()
        self.BTP.click_zhuihao_tap()
        self.BTP.getImage

    def test_bjrb_fj(self):
        """测试房间模式  北京幸运28"""
        self.driver.get("http://m.tcuat0.com")
        self.driver.find_element_by_class_name("icon-gc").click()
        sleep(2)
        self.BTP.click_fangjian()
        self.BTP.click_fj_bjrb()
        self.BTP.click_hs_VIP_01()
        self.BTP.click_fj_bjrb_tz_area()
        self.BTP.click_fj_bjrb_dxds()
        self.BTP.click_queren_btn()
        self.BTP.getImage


###########################################################################################测试悉尼幸运28投注追号

    def test_xnrb_tz(self):
        """测试传统模式 悉尼幸运28投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)
        self.BTP.click_fj_bjrb()



    def test_jndrb_tz(self):
        """测试传统模式 加拿大幸运28投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_twrb_tz(self):
        """测试传统模式 台湾幸运28投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_lhc_tz(self):
        """测试传统模式 六合彩投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_jlprb_tz(self):
        """测试传统模式 吉隆坡28投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_ffc_tz(self):
        """测试传统模式 分分彩投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_hlydwf_tz(self):
        """测试传统模式 荷兰1.5分彩投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_flbsf_tz(self):
        """测试传统模式 菲律宾三分彩投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_flbwf_tz(self):
        """测试传统模式 菲律宾五分彩投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)


    def test_wfc_tz(self):
        """测试传统模式 五分彩投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)


    def test_cqssc_tz(self):
        """测试传统模式 重庆时时彩投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)


    def test_tjssc_tz(self):
        """测试传统模式 天津时时彩投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_xjssc_tz(self):
        """测试传统模式 新疆时时彩投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_jxsyxw_tz(self):
        """测试传统模式 江西11选5投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_jssyxw_tz(self):
        """测试传统模式 江苏11选5投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_gdsyxw_tz(self):
        """测试传统模式 广东11选5投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)


    def test_sdsyxw_tz(self):
        """测试传统模式 山东11选5投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_ahsyxw_tz(self):
        """测试传统模式 安徽11选5投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)


    def test_shsyxw_tz(self):
        """测试传统模式 11选5投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)


    def test_xyft_tz(self):
        """测试传统模式 幸运飞艇投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_jssc_tz(self):
        """测试传统模式 极速赛车投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)

    def test_bjsc_tz(self):
        """测试传统模式 北京赛车投注"""
        TestLogin.login(self)
        driver = self.driver
        url="http://m.tcuat0.com/category"
        self.BTP = BetsPage(driver,url)#声明self.BTP对象
        driver.find_element_by_class_name("icon-gc").click()
        sleep(2)


if __name__ == "__main__":
      unittest.main()