# _*_ coding:utf-8 _*_
from time import sleep

from H5.test_case.page_objs.home.homePage import HomePage
from H5.test_case.page_objs.home.loginPage import LoginPage
from util.Logs import Logger
from util.readJson import readJson

mylog = Logger(logger='=public_case').getlog()

class Public(object):
    def login(self):
        '''登录'''
        driver = self.driver
        rj = readJson()
        data = rj.load_data("login.json")
        mylog.info("读取json文件内容")
        url = data["URL"]
        username = data["userName"]
        password = data["userPwd"]
        LP = LoginPage(driver, url)  # 声明LP对象
        LP.gotoLoginPage()
        LP.input_username(username)
        LP.input_password(password)
        LP.click_login_btn()
        sleep(3)
        HP = HomePage(driver, url)  # 声明HP对象
        HP.click_message_btn()


