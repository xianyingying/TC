import unittest
from time import sleep
from selenium import webdriver
from H5.test_case.page_objs.recharge.rechargePage import RechargePage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_recharge').getlog()

class TestRecharge(unittest.TestCase):
    '''个人中心'''
    driver = TestLogin.driver
    url = "http://m.tcuat0.com/recharge"
    RP = RechargePage(driver, url)  # 声明RP对象

    def test_online_recharge(self):
        '''测试线上充值'''
        self.driver.get("http://m.tcuat0.com")
        self.driver.find_element_by_class_name("icon-cz").click()
        sleep(2)
        self.RP.input_recharge_money(100)
        self.RP.click_online_first()
        self.RP.click_first_channel()
        self.RP.click_online_submit_btn()
        self.RP.getImage

    def test_offline_recharge(self):
        '''测试线下充值'''
        self.driver.get("http://m.tcuat0.com")
        self.driver.find_element_by_class_name("icon-cz").click()
        sleep(2)
        self.RP.input_recharge_money(50)
        self.RP.click_offline_first()
        self.RP.click_first_channel()
        self.RP.select_bank()
        self.RP.input_name("张三")
        self.RP.click_offline_submit_btn()
        self.RP.getImage


if __name__ == "__main__":
      unittest.main()