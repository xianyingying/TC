import unittest
from time import sleep
from selenium import webdriver
from H5.test_case.page_objs.user.vipgradePage import VipGradePage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_vip_grade').getlog()

class TestVipGrade(unittest.TestCase):
    '''用户等级'''

    driver = TestLogin.driver
    url = "http://m.tcuat0.com/vipgrade"
    VGP = VipGradePage(driver, url)  # 声明VGP对象

    def test_user_level(self):
        '''测试用户等级'''
        self.driver.get("http://m.tcuat0.com/")
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.VGP.click_user_level()
        sleep(2)
        self.VGP.click_tequan()
        self.VGP.click_hall_rule()
        mylog.info("点击查看我当前等级的回水说明")
        self.VGP.getImage
'''
        #########################################################幸运28
        self.VGP.click_rule_filter()
        mylog.info("点击筛选按钮")
        self.VGP.click_caipiao()
        self.VGP.click_queren()
        mylog.info("查看彩票 幸运28类 回水厅 回水规则")

        self.VGP.click_rule_filter()
        self.VGP.click_baoben()
        self.VGP.click_queren()
        mylog.info("查看彩票 幸运28类 保本厅 回水规则")

        self.VGP.click_rule_filter()
        self.VGP.click_gaopeilv()
        self.VGP.click_queren()
        mylog.info("查看彩票 幸运28类 高赔率厅 回水规则")

        self.VGP.click_rule_filter()
        self.VGP.click_chuantong()
        self.VGP.click_queren()
        mylog.info("查看彩票 幸运28类 传统模式 回水规则")

        #########################################################时时彩
        self.VGP.click_rule_filter()
        self.VGP.click_ssc()
        self.VGP.click_queren()
        mylog.info("查看彩票 时时彩类 回水厅 回水规则")

        self.VGP.click_rule_filter()
        self.VGP.click_ssc()
        self.VGP.click_baoben()
        self.VGP.click_queren()
        mylog.info("查看彩票 时时彩类 保本厅 回水规则")

        self.VGP.click_rule_filter()
        self.VGP.click_ssc()
        self.VGP.click_gaopeilv()
        self.VGP.click_queren()
        mylog.info("查看彩票 时时彩类 高赔率厅 回水规则")


        self.VGP.click_rule_filter()
        self.VGP.click_ssc()
        self.VGP.click_chuantong()
        self.VGP.click_queren()
        mylog.info("查看彩票 时时彩类 传统模式 回水规则")

        #########################################################赛车
        self.VGP.click_rule_filter()
        self.VGP.click_sc()
        self.VGP.click_queren()
        mylog.info("查看彩票 赛车 回水厅 回水规则")

        self.VGP.click_rule_filter()
        self.VGP.click_sc()
        self.VGP.click_baoben()
        self.VGP.click_queren()
        mylog.info("查看彩票 赛车 保本厅 回水规则")

        self.VGP.click_rule_filter()
        self.VGP.click_sc()
        self.VGP.click_gaopeilv()
        self.VGP.click_queren()
        mylog.info("查看彩票 赛车 高赔率厅 回水规则")


        self.VGP.click_rule_filter()
        self.VGP.click_sc()
        self.VGP.click_chuantong()
        self.VGP.click_queren()
        mylog.info("查看彩票 赛车 传统模式 回水规则")
        #########################################################快3
        self.VGP.click_rule_filter()
        self.VGP.click_ks()
        self.VGP.click_queren()
        mylog.info("查看彩票 快3 回水厅 回水规则")

        self.VGP.click_rule_filter()
        self.VGP.click_ks()
        self.VGP.click_baoben()
        self.VGP.click_queren()
        mylog.info("查看彩票 快3 保本厅 回水规则")

        self.VGP.click_rule_filter()
        self.VGP.click_ks()
        self.VGP.click_gaopeilv()
        self.VGP.click_queren()
        mylog.info("查看彩票 快3 高赔率厅 回水规则")


        self.VGP.click_rule_filter()
        self.VGP.click_ks()
        self.VGP.click_chuantong()
        self.VGP.click_queren()
        mylog.info("查看彩票 快3 传统模式 回水规则")
        #########################################################11选5
        self.VGP.click_rule_filter()
        self.VGP.click_syxw()
        self.VGP.click_queren()
        mylog.info("查看彩票 11选5 回水厅 回水规则")

        self.VGP.click_rule_filter()
        self.VGP.click_syxw()
        self.VGP.click_baoben()
        self.VGP.click_queren()
        mylog.info("查看彩票 11选5 保本厅 回水规则")

        self.VGP.click_rule_filter()
        self.VGP.click_syxw()
        self.VGP.click_gaopeilv()
        self.VGP.click_queren()
        mylog.info("查看彩票 11选5 高赔率厅 回水规则")


        self.VGP.click_rule_filter()
        self.VGP.click_syxw()
        self.VGP.click_chuantong()
        self.VGP.click_queren()
        mylog.info("查看彩票 11选5 传统模式 回水规则")
        #########################################################六合彩
        self.VGP.click_rule_filter()
        self.VGP.click_lhc()
        self.VGP.click_queren()
        mylog.info("查看彩票 六合彩 回水厅 回水规则")

        self.VGP.click_rule_filter()
        self.VGP.click_caipiao()
        self.VGP.click_lhc()
        self.VGP.click_baoben()
        self.VGP.click_queren()
        mylog.info("查看彩票 六合彩 保本厅 回水规则")

        self.VGP.click_rule_filter()
        self.VGP.click_caipiao()
        self.VGP.click_lhc()
        self.VGP.click_huishui()
        self.VGP.click_queren()
        mylog.info("查看彩票 六合彩 高赔率厅 回水规则")


        self.VGP.click_rule_filter()
        self.VGP.click_caipiao()
        self.VGP.click_lhc()
        self.VGP.click_gaopeilv()
        self.VGP.click_queren()
        mylog.info("查看彩票 六合彩 传统模式 回水规则")

      #########################################################PT
        self.VGP.click_rule_filter()
        self.VGP.click_dianzi()
        self.VGP.click_pt()
        self.VGP.click_queren()
        mylog.info("查看电子 PT 回水规则")

        #########################################################FG
        self.VGP.click_rule_filter()
        self.VGP.click_dianzi
        self.VGP.click_fg()
        self.VGP.click_queren()
        mylog.info("查看电子 FG 回水规则")

        #########################################################AG
        self.VGP.click_rule_filter()
        self.VGP.click_shixun()
        self.VGP.click_ag()
        self.VGP.click_queren()
        mylog.info("查看视讯 AG 回水规则")

        #########################################################LEBO
        self.VGP.click_rule_filter()
        self.VGP.click_shixun()
        self.VGP.click_lebo()
        self.VGP.click_queren()
        mylog.info("查看视讯 lebo 回水规则")

        #########################################################沙巴体育
        self.VGP.click_rule_filter()
        self.VGP.click_tiyu()
        self.VGP.click_sbty()
        self.VGP.click_queren()
        mylog.info("查看体育 沙巴体育 回水规则")

        #########################################################SBBENCH
        self.VGP.click_rule_filter()
        self.VGP.click_tiyu()
        self.VGP.click_sbbench()
        self.VGP.click_queren()
        mylog.info("查看体育 SBBENCH 回水规则")
        #################################################################

        self.VGP.back()
'''

if __name__ == "__main__":
      unittest.main()