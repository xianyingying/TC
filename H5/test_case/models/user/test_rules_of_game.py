import unittest
from selenium import webdriver
from H5.test_case.page_objs.user.gameRulePage import GameRulePage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_rule_of_game').getlog()

class TestRuleOfGame(unittest.TestCase):
    '''游戏规则'''
    driver = TestLogin.driver
    url = 'http://m.tcuat0.com/help'
    GP = GameRulePage(driver, url)

    def test_game_rule(self):
        """测试游戏规则"""
        self.driver.get("http://m.tcuat0.com")
        self.driver.find_element_by_class_name('icon-wd').click()
        self.GP.click_game_rule()
        self.GP.getImage

if __name__ == "__main__":
      unittest.main()