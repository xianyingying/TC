import unittest
from H5.test_case.page_objs.user.odersPage import OrdersPage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin
from time import sleep

mylog = Logger(logger='=test_order').getlog()

class TestOrder(unittest.TestCase):
    '''投注记录'''
    driver = TestLogin.driver
    url = 'http://m.tcuat0.com/help'
    OP = OrdersPage(driver, url)

    def goto_order_page(self):
        self.driver.get("http://m.tcuat0.com/")
        self.driver.find_element_by_class_name('icon-wd').click()
        sleep(2)
        self.driver.find_element_by_class_name("icon-touzhujilu").click()
        sleep(2)

    def test_tz_record(self):
        """测试投注记录"""
        self.goto_order_page()
        self.OP.click_tz_record()
        self.OP.getImage

    def test_zh_record(self):
        """测试追号记录"""
        self.goto_order_page()
        self.OP.click_zh_record()
        self.OP.getImage

if __name__ == "__main__":
      unittest.main()