import unittest
from time import sleep
from H5.test_case.page_objs.user.userPage import UserPage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_user').getlog()

class TestUser(unittest.TestCase):
    '''个人中心'''
    driver = TestLogin.driver
    url = "http://m.tcuat0.com/user"
    UP = UserPage(driver, url)  # 声明UP对象
    
    def test_skip_to_luck_draw(self):
        '''测试跳转到幸运轮盘'''
        self.driver.get("http://m.tcuat0.com")
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.UP.click_lucky_draw()
        mylog.info("点击幸运轮盘，检查页面跳转")
        self.UP.getImage

    def test_skip_to_recharge(self):
        '''测试跳转到充值页面'''
        self.driver.get("http://m.tcuat0.com")
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.UP.click_recharge()
        mylog.info("点击充值按钮，检查页面跳转")
        self.UP.getImage

    def test_skip_to_user_level(self):
        '''测试跳转到用户等级'''
        self.driver.get("http://m.tcuat0.com")
        sleep(2)
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.UP.click_user_level()
        mylog.info("点击用户等级，检查页面跳转")
        self.UP.getImage

    def test_skip_to_transfer(self):
        '''测试跳转到额度转换'''
        self.driver.get("http://m.tcuat0.com")
        sleep(2)
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.UP.click_transfer()
        mylog.info("点击额度转换，检查页面跳转")
        self.UP.getImage

    def test_skip_to_my_team(self):
        '''测试跳转到我的团队'''
        self.driver.get("http://m.tcuat0.com")
        sleep(2)
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.UP.click_my_team()
        mylog.info("点击我的团队，检查页面跳转")
        self.UP.getImage

    def test_skip_to_message_notification(self):
        '''测试跳转到消息公告'''
        self.driver.get("http://m.tcuat0.com")
        sleep(2)
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.UP.click_message_notification()
        mylog.info("点击消息公告，检查页面跳转")
        self.UP.getImage

    def test_skip_to_safety_center(self):
        '''测试跳转到安全中心'''
        self.driver.get("http://m.tcuat0.com")
        sleep(2)
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.UP.click_safety_center()
        mylog.info("点击安全中心，检查页面跳转")
        self.UP.getImage

    def test_skip_to_customer_service(self):
        '''测试跳转到联系客服'''
        self.driver.get("http://m.tcuat0.com")
        sleep(2)
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.UP.click_customer_service()
        mylog.info("点击联系客服，检查页面跳转")
        self.UP.getImage

    def test_skip_to_rebate(self):
        '''测试跳转到回水规则'''
        self.driver.get("http://m.tcuat0.com")
        sleep(2)
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.UP.click_rebate()
        mylog.info("点击回水规则，检查页面跳转")
        self.UP.getImage

    def test_skip_to_rules_of_the_game(self):
        '''测试跳转到游戏规则'''
        self.driver.get("http://m.tcuat0.com")
        sleep(2)
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.UP.slide_down_from_user_pic(700)
        self.UP.click_rules_of_the_game()
        mylog.info("点击游戏规则，检查页面跳转")
        self.UP.getImage

    def test_skip_to_setting(self):
        '''测试跳转到设置页面'''
        self.driver.get("http://m.tcuat0.com")
        sleep(2)
        sleep(2)
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.UP.slide_down_from_user_pic(800)
        self.UP.click_setting()
        mylog.info('点击设置，检查页面跳转')
        self.UP.getImage

    def test_cash_withdrawal(self):
        """测试提现"""
        pass


if __name__ == "__main__":
      unittest.main()