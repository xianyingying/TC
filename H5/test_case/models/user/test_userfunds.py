import unittest
from time import sleep
from selenium import webdriver
from H5.test_case.page_objs.user.userfundsPage import UserFundsPage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_userfunds').getlog()

class TestUserFunds(unittest.TestCase):
    '''资金明细'''

    driver = TestLogin.driver
    url = "http://m.tcuat0.com/LuckDraw"
    UFP = UserFundsPage(driver, url)  # 声明UFP对象

    def test_user_funds(self):
        '''测试资金明细'''
        self.driver.get("http://m.tcuat0.com/")
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.driver.find_element_by_class_name("icon-wode-jifen").click()
        sleep(2)
        self.UFP.click_filter()
        self.UFP.click_income()
        self.UFP.click_sure_btn()
        self.UFP.getImage


if __name__ == "__main__":
      unittest.main()