import unittest
from selenium import webdriver
from time  import sleep
from H5.test_case.page_objs.user.servicePage import ServicePage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_customer_service').getlog()

class TestCustomerService(unittest.TestCase):
    '''客服系统'''
    driver = TestLogin.driver
    url = "http://m.tcuat0.com/service"
    SP = ServicePage(driver, url)
    def test_contact_service(self):
        self.driver.get("http://m.tcuat0.com")
        self.driver.find_element_by_class_name('icon-wd').click()
        sleep(2)
        self.driver.find_element_by_class_name('icon-kefu').click()
        sleep(2)
        self.SP.contact_service("111")
        sleep(2)
        self.SP.getImage

if __name__ == "__main__":
      unittest.main()