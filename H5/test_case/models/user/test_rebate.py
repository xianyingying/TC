import unittest
from H5.test_case.page_objs.user.hallrulePage import HallRulePage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_rebate').getlog()

class TestRebate(unittest.TestCase):
    '''回水规则'''
    driver = TestLogin.driver
    url = "http://m.tcuat0.com/hallrule"
    HRP = HallRulePage(driver, url)

    def test_hall_rule(self):
        """测试查看回水规则"""
        self.driver.get("http://m.tcuat0.com")
        self.driver.find_element_by_class_name("icon-wd").click()
        self.HRP.click_hall_rule()
        self.HRP.getImage


if __name__ == "__main__":
      unittest.main()