import unittest
from time import sleep
from H5.test_case.page_objs.user.messagePage import MessagePage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_message_notification').getlog()

class TestMessageNotification(unittest.TestCase):
    '''消息公告'''
    driver = TestLogin.driver
    url = "http://m.tcuat0.com/message"
    MP = MessagePage(driver, url)  # 声明MP对象

    def goto_message(self):
        self.driver.get("http://m.tcuat0.com")
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(1)
        self.driver.find_element_by_class_name("icon-xiaoxigonggao").click()
        sleep(1)

    def test_all_massage_are_marked_as_read(self):
        """测试标志全部消息为已读"""
        self.goto_message()
        self.MP.all_massage_are_marked_as_read()
        self.MP.getImage


    def test_delete_all_message(self):
        """删除全部消息"""
        self.goto_message()
        self.MP.delete_all_message()
        self.MP.getImage

if __name__ == "__main__":
      unittest.main()