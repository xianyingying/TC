import unittest
from time import sleep
from selenium import webdriver
from H5.test_case.page_objs.user.safetyPage import SafetyPage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_safety').getlog()

class TestSafety(unittest.TestCase):
    '''安全中心'''
    driver = TestLogin.driver
    url = 'http://m.tcuat0.com/user'
    SP = SafetyPage(driver, url)


    def goto_anquan(self):
        self.driver.get("http://m.tcuat0.com")
        self.driver.find_element_by_class_name('icon-wd').click()
        sleep(2)
        self.driver.find_element_by_class_name('icon-anquan').click()
        sleep(2)

    def test_id_auth(self):
        """测试实名认证"""
        self.goto_anquan()
        self.SP.id_auth("胡夏柳","370101197602092780")
        self.SP.getImage


    def test_reset_pwd(self):
        """测试修改登录密码"""
        self.goto_anquan()
        self.SP.set_tixian_pwd("1234","1234")
        self.SP.getImage



    def test_set_tixian_pwd(self):
        """测试修改提现密码"""
        self.goto_anquan()
        self.SP.set_tixian_pwd("1234","1234")
        self.SP.getImage

    def test_binding_phone_number(self):
        """修改绑定手机"""
        self.goto_anquan()
        self.SP.binding_phone_number("13751056424","1234")
        self.SP.getImage

    def test_binding_email_add(self):
        """测试绑定邮箱"""
        self.goto_anquan()
        self.SP.test_binding_email_add("304547577@qq.com","1234")
        self.SP.getImage


if __name__ == "__main__":
      unittest.main()