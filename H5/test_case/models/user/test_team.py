import unittest
from time import sleep
from selenium import webdriver
from H5.test_case.page_objs.user.teamPage import TeamPage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_my_team').getlog()

class TestMyTeam(unittest.TestCase):
    '''我的团队'''
    driver = TestLogin.driver
    url = "http://m.tcuat0.com/LuckDraw"
    TP = TeamPage(driver, url)  # 声明TP对象
    def test_my_team(self):
        '''测试我的团队'''
        self.driver.get("http://m.tcuat0.com/")
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.driver.find_element_by_class_name("icon-wodetuandui").click()
        sleep(2)
        self.TP.click_tjxj_btn()
        self.TP.input_account()
        self.TP.input_password()
        self.TP.input_repeat_password()
        self.TP.click_sure_btn()
        self.TP.getImage

if __name__ == "__main__":
      unittest.main()