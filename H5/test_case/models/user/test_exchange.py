import unittest
from time import sleep
from H5.test_case.page_objs.user.exchangePage import ExchangePage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_transfer').getlog()

class TestTransfer(unittest.TestCase):
    '''额度转换'''
    driver = TestLogin.driver
    url = "http://m.tcuat0.com/exchange"
    EP = ExchangePage(driver, url)

    def test_transfer(self):
        """测试额度转换"""
        self.driver.get("http://m.tcuat0.com/")
        self.driver.find_element_by_class_name('icon-wd').click()
        sleep(3)
        self.EP.click_transfer_btn()
        self.EP.click_in_wallet()
        self.driver.find_element_by_css_selector('#vux-popup-picker-gltfa > div > div.vux-popup-header.vux-1px-b > div.vux-popup-header-right').click()
        self.EP.getImage

if __name__ == "__main__":
      unittest.main()