import unittest
from time import sleep
from selenium import webdriver
from H5.test_case.page_objs.user.LuckDrawPage import LuckyDrawPage
from util.Logs import Logger
from H5.test_case.models.home.test_login import TestLogin

mylog = Logger(logger='=test_luck_draw').getlog()

class TestLuckyDraw(unittest.TestCase):
    '''幸运轮盘'''

    driver = TestLogin.driver
    url = "http://m.tcuat0.com/LuckDraw"
    LDP = LuckyDrawPage(driver, url)  # 声明UP对象


    def goto_luck_draw(self):
        self.driver.get("http://m.tcuat0.com")
        sleep(2)
        self.driver.find_element_by_class_name("icon-wd").click()
        sleep(2)
        self.driver.find_element_by_class_name("luckDrawBtn").click()
        sleep(2)


    def test_lucky_draw(self):
        '''测试轮盘抽奖'''
        self.goto_luck_draw()
        self.LDP.click_start_btn()
        mylog.info("点击幸运轮盘Go按钮，检查轮盘抽奖")
        self.LDP.getImage

    def test_close_luck_tip(self):
        """测试关闭中奖提示弹框"""
        self.goto_luck_draw()
        self.LDP.click_start_btn()
        self.LDP.click_tip_close_btn()
        mylog.info("点击中奖提示弹框关闭按钮")
        self.LDP.getImage

    def test_check_winning_list(self):
        """测试查看我的奖品列表"""
        self.goto_luck_draw()
        self.LDP.click_winning_list()
        mylog.info("点击我的奖品按钮")
        self.LDP.getImage



if __name__ == "__main__":
      unittest.main()