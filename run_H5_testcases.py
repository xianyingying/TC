import time
import unittest
import util.HTMLTestReportCN1
#========================首页============================
from H5.test_case.models.home.test_login import TestLogin
from H5.test_case.models.home.test_register import TestRegister
#========================充值============================
from H5.test_case.models.recharge.test_recharge import TestRecharge
#========================购彩============================
from H5.test_case.models.bets.test_bets import TestBets
#=======================开奖走势==========================
from H5.test_case.models.history.test_lottery_trend import TestLotteryPrize
#=======================个人中心==========================
from H5.test_case.models.user.test_exchange import TestTransfer
from H5.test_case.models.user.test_luck_draw import TestLuckyDraw
from H5.test_case.models.user.test_team import TestMyTeam
from H5.test_case.models.user.test_user import TestUser
from H5.test_case.models.user.test_userfunds import TestUserFunds
from H5.test_case.models.user.test_orders import TestOrder
from H5.test_case.models.user.test_vipgrade import TestVipGrade
from H5.test_case.models.user.test_message import TestMessageNotification
from H5.test_case.models.user.test_safety import TestSafety
from H5.test_case.models.user.test_service import TestCustomerService
from H5.test_case.models.user.test_rebate import TestRebate
from H5.test_case.models.user.test_rules_of_game import TestRuleOfGame
#----------------------------------------------------------------------------------------------------------------

help(TestLogin)

suite = unittest.TestSuite()
#----------------------------------------------------------------------------------------------------------------
#==========================首页==================================
suite.addTest(TestLogin('test_login'))     #登录
# suite.addTest(TestLogin('test_tryplay_login'))     #试玩登录
# suite.addTest(TestRegister('test_register'))    #用户注册

# =========================充值===================================
suite.addTest(TestRecharge('test_online_recharge'))     #线上充值
suite.addTest(TestRecharge('test_offline_recharge'))    #线下充值

#==========================购彩===================================
suite.addTest(TestBets('test_bjrb_tz'))     #北京28投注
suite.addTest(TestBets('test_bjrb_zh'))    #北京28追号
suite.addTest(TestBets('test_bjrb_fj'))    #房间模式北京28

#===========================开奖走势===============================
suite.addTest(TestLotteryPrize('test_skip_to_history'))   #历史走势
suite.addTest(TestLotteryPrize('test_qgc'))     #去购彩跳转

# ==========================个人中心===============================
suite.addTest(TestUser('test_skip_to_luck_draw'))     #个人中心跳转
suite.addTest(TestUser('test_skip_to_recharge'))     #个人中心跳转
suite.addTest(TestUser('test_skip_to_user_level'))     #个人中心跳转
suite.addTest(TestUser('test_skip_to_transfer'))     #个人中心跳转
suite.addTest(TestUser('test_skip_to_my_team'))     #个人中心跳转
suite.addTest(TestUser('test_skip_to_message_notification'))     #个人中心跳转
suite.addTest(TestUser('test_skip_to_safety_center'))     #个人中心跳转
suite.addTest(TestUser('test_skip_to_customer_service'))     #个人中心跳转
suite.addTest(TestUser('test_skip_to_rebate'))     #个人中心跳转
suite.addTest(TestUser('test_skip_to_rules_of_the_game'))     #个人中心跳转
suite.addTest(TestUser('test_skip_to_setting'))     #个人中心跳转
suite.addTest(TestLuckyDraw('test_lucky_draw'))   #幸运轮盘
suite.addTest(TestLuckyDraw('test_close_luck_tip'))   #幸运轮盘
suite.addTest(TestLuckyDraw('test_check_winning_list'))   #幸运轮盘
suite.addTest(TestVipGrade('test_user_level'))   #用户等级
suite.addTest(TestTransfer('test_transfer'))   #额度转换
suite.addTest(TestUserFunds('test_user_funds'))     #资金明细
suite.addTest(TestOrder('test_tz_record'))   #投注记录
suite.addTest(TestOrder('test_zh_record'))   #追号记录
suite.addTest(TestMyTeam('test_my_team'))   #我的团队
suite.addTest(TestMessageNotification('test_all_massage_are_marked_as_read'))    #标志所有消息为已读
suite.addTest(TestMessageNotification('test_delete_all_message'))    #删除所有消息
suite.addTest(TestSafety('test_id_auth'))      #实名认证
suite.addTest(TestSafety('test_reset_pwd'))       #重设登录密码


suite.addTest(TestSafety('test_set_tixian_pwd'))   #设置提现密码
suite.addTest(TestSafety('test_binding_phone_number'))  #绑定手机号
suite.addTest(TestSafety('test_binding_email_add'))   #绑定邮箱
suite.addTest(TestCustomerService('test_contact_service'))   #联系客服
suite.addTest(TestRebate('test_hall_rule'))  #回水规则
suite.addTest(TestRuleOfGame('test_game_rule'))  #游戏规则
#-----------------------------------------------------------------------------------------------------------------
suite.addTest(TestLogin('test_logout'))     #登出

report_time = time.strftime("%Y-%m-%d", time.localtime(time.time()))
filePath ='D:/TC/H5/report/htmlReport/'+report_time+'.html'
fp = open(filePath,'wb')
runner = util.HTMLTestReportCN1.HTMLTestRunner(stream=fp, title='H5自动化测试报告')
runner.run(suite)



